/****
rpsd - rock paper sissors daemon
****/
#include <stdio.h>      // fprintf
#include <stdlib.h>     // close, exit
#include <string.h>     // For memset.
#include <sys/types.h>  // STD C Types.
#include <sys/socket.h> // For Berkley Sockets
#include <netinet/in.h> // sockaddr_in structure.
#include <unistd.h>     // POSIX

#define PAPER           'p'
#define ROCK            'r'
#define SCISSORS        's'

// Server stuff.
#define LISTEN_BACKLOG  8
#define MAX_USERS       8
#define MAX_ROUNDS      1000

// Its used in the book, but somehow it should be in one of those includes.
#define MAX(a,b)         ((a < b) ?  (b) : (a))

/****
  Result - returns the player one result from rps
****/
int results(char p1, char p2) {
  switch(p1) {
    case ROCK:
      switch(p2) {
        case ROCK:      return 0;  // Rock ties Rock
        case PAPER:     return -1; // Paper covers Rock
        case SCISSORS:  return 1;  // Rock breaks Scissors
        default: return 0;         // Uneligible answer.
      }
    case PAPER: // Paper beats rock.
      switch(p2) {
        case ROCK:      return 1;  // Paper covers Rock
        case PAPER:     return 0;  // Paper ties Paper
        case SCISSORS:  return -1; // Scissors cuts Paper
        default: return 0;         // Uneligible answer.
      }
    case SCISSORS:
      switch(p2) {
        case ROCK:      return -1; // Rock breaks Scissors
        case PAPER:     return 1;  // Scissors cuts Paper
        case SCISSORS:  return 0;  // Scissors ties Scissors
        default: return 0;         // Uneligible answer.
      }
    default: return 0;             // Uneligible answer.
  }

  // Just in case.
  return 0;
}
/***
  ReceiveAnser - grabs answer from client.
    Parameters: i - for loop index,
                sock, sock_pile,buf, - socket read stuff.
                pli - player index (the number of answers they've sent).
                pln - player number 1 or 2.
    Returns: 1 on receiving an answer.
***/
int receiveAnswer(int i, int *sock, fd_set *sock_pile, char *buf, int *pli, int pln) {
  if(FD_ISSET(*sock, sock_pile)) {
    if(*pli < MAX_ROUNDS) {
      read(*sock,buf,1);
      fprintf(stderr,"RECEIVED: [%d] => [%d][%d]: %c\n",i,pln,*pli,buf[0]);
      *pli = *pli + 1;
      return 1;
    }
  }
  return 0; 
}
/***
  Send Results - writes the results to the client.
  Basically    - handles the select bs for us here.
***/
void sendResults(int *sock, char *buf, size_t bytes) {
    fd_set sock_pile;       // A file descriptor set for select to use.
    int maxFD         = -1; // Highest fileno from an fd in the set.
    // Select socket.
    FD_ZERO(&sock_pile);
    FD_SET(*sock, &sock_pile);
    maxFD = MAX(maxFD, *sock);
    if(( select(maxFD + 1, NULL, &sock_pile, NULL, NULL)) == -1) {
      perror("Communications Breakdown");
      return;
    }
    if(write(*sock, buf,bytes) == -1) {
      perror("write");
      return;
    }
    
    return;
}
/***
  Handles one round of rps.
***/
void rpsRound(int i, int *p1in, int *p2in, char p1Ans, char p2Ans, int *p1total, int *p2total) {

    static char p1results[5];
    static char p2results[5];

    switch(results(p1Ans,p2Ans)) {
      case 1: //Player 1 wins.
        p1results[0] = '1';
        *p1total = *p1total + 1;
        p2results[0] = '0';
        break;
      case -1: //Player 2 wins.
        p1results[0] = '0';
        p2results[0] = '1';
        *p2total++ = *p2total + 1;
        break;
      default: //Neither win.
        p1results[0] = '0';
        p2results[0] = '0';
    }
        
      
    // Write the results back to the clients.
    // In the format of: Score, P1 Ans, P2 Ans. 
    // 5 characters.
    p1results[1] = ',';
    p1results[2] = p1Ans;
    p1results[3] = ',';
    p1results[4] = p2Ans;

    p2results[1] = ',';
    p2results[2] = p2Ans;
    p2results[3] = ',';
    p2results[4] = p1Ans;

    sendResults(p1in, p2results, 5);
    sendResults(p2in, p2results, 5);
    return;
}

/*****
  startGame
    - Forks a child to handle a game between two players.
    - Sets up the main game loop.
      - Wait for input. 
      - Get the results from the input. 
        (And write results back.)
      - Tally the score.
    - Send final results 
    - Then close the sockets and exit.
***/
int startGame(int p1sock, int p2sock) {

  int         i           = 0;        // for loop index.
  int         maxFD       = -1;       // largest fileno from sockets. Used in select.
  int         p1Answered[MAX_ROUNDS]; // bit that is set if we received input from player.
  int         p2Answered[MAX_ROUNDS]; // see above.
  static char p1buf[MAX_ROUNDS][1];   // Holds the client input 'r'|'p'|'s'
  static char p2buf[MAX_ROUNDS][1];   // See Above.
  int         p1i         = 0;        // player 1 input index.
  int         p2i         = 0;        // player 2 input index.
  static char p1result[1] = "0";      // buffer to write the win/loss result back.
  static char p2result[1] = "0";      // 0 = Loss, 1 = Win.
  int         p1total     = 0;        // Player 1 total score.
  int         p2total     = 0;        // Player 2 total score.
  pid_t       pid         = 0;        // process ID of the child.
  fd_set      sockPile;               // Pile of sockets to select from.


  // Fork the listener
  switch((pid = fork())) {
    case -1:
      perror("What the fork? ");
      exit(6);
    case 0: /* Child */
      fprintf(stderr, "Started Game\n");
      for(i = 0;i < MAX_ROUNDS; i++) {
        if(p1i <= i) {
          p1Answered[i] = 0;
        }
        if(p2i <= i) {
          p2Answered[i] = 0;
        } 
        while((p1Answered[i] + p2Answered[i]) < 2) {    
          FD_ZERO(&sockPile);
          FD_SET(p1sock, &sockPile);
          maxFD = MAX(maxFD, p1sock);
          FD_SET(p2sock, &sockPile);
          maxFD = MAX(maxFD, p2sock);

          if(( select(maxFD + 1, &sockPile, NULL, NULL, NULL)) == -1) {
            perror("Selection Problem");
            continue;
          }

         
          // Check if its player one
          p1Answered[p1i] = receiveAnswer(i, &p1sock, &sockPile, p1buf[p1i],&p1i,1);

          // Check if its player two
          p2Answered[p2i] = receiveAnswer(i, &p2sock, &sockPile, p2buf[p2i],&p2i,1);
        }
        // Now that we have input... start the round. 
        rpsRound(i, &p1sock, &p2sock,p1buf[i][0],p2buf[i][0],&p1total, &p2total);
      }

      // Send the final result back.
      if(p1total > p2total) {
        p1result[0] = '1';
      }
      else if(p2total > p1total) {
        p2result[0] = '1';
      }

      sendResults(&p1sock, p1result, 1);
      sendResults(&p1sock, p1result, 1);
      close(p1sock);
      close(p2sock);

      exit(0);
    default: /* Parent */
      close(p1sock);
      close(p2sock);
      return 0;
  }
  
  // Should not be here
  perror("I shouldn't be here today!"); 
  exit(7);
}
// Step 1: Have it take two programs named player one and player two and establish I/O with them.
int main(int argc,char **argv) {

  pid_t w    = 0;
  int   status = 0;
  int   i      = 0;
  // static char junk[1];

  // Socket time!
  // eventually this should be a -p argument
  int port     = 6112;
  int oldSock  = -1;
  int yes      = 1;

  if((oldSock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("Unable to make socket connection");
    exit(1);
  }

  /****
    Reuse the address, to prevent waiting for the kernel if the program crashes.
  ***/

  if(( setsockopt(oldSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1) {
    perror("Could not use setsockopt to reuse address.");
    close(oldSock);
    exit(2);
  }

  /***
    Bind the socket to a port.
  ***/
  {
    struct sockaddr_in address;
    memset(&address, 0, sizeof(address)); // Clear the structure.

    address.sin_family      = AF_INET;                    // Address type.
    address.sin_port        = htons(port);                // Port
    address.sin_addr.s_addr = htonl (INADDR_ANY);         // Any address this host has.
    memset(address.sin_zero, 0, sizeof(address.sin_zero)); // Zero out sin_zero? hmmm...
    
    if(( bind(oldSock, (struct sockaddr *)&address, sizeof(address))) == -1) {
      perror("The binding has failed, run for your lives!");
      close(oldSock);
      exit(3);
    } 
  }

  /***
    Listen to the port
  ***/
  if(( listen(oldSock, LISTEN_BACKLOG)) == -1) {
    perror("You're not listening!");
    close(oldSock);
    exit(4);
  }

  /***
    Now loop through for connections - forever.
  ***/

  int     smellySocks[MAX_USERS];       // Client's socks.
  i = 0;
  do {
    if(i < MAX_USERS) {
      // Setup client address.
      struct  sockaddr_in address;        // Client address.
    
      memset(&address, 0, sizeof(address)); // Clear the structure.
      socklen_t addressLength = sizeof(address); 
   
      // 1. Wait for a connection.
      if(( smellySocks[i] = accept(oldSock, (struct sockaddr *)&address, &addressLength)) == -1) {
        perror("Unacceptible!");
        close(oldSock);
        exit(5);
      }
      fprintf(stderr, "Accepted [%d]\n", i);
      // 2. If we have 2 players, start a game.
      if( (i % 2) == 1) {
        startGame(smellySocks[i - 1],smellySocks[i]);
        close(smellySocks[i - 1]);
        close(smellySocks[i]);
      }
      i++;
    }
    // Otherwise wait for any children like a good parent. 
    else {
        /**
          Now the parent waits
        **/

        while((w = wait(&status)) && w != -1) {
          fprintf(stderr,"Wait on PID: %d returns status of %04X\n",w,status);
        }
        i = 0;
    }

  } while(1); // Always true.
  close(oldSock);
}
