# rpsd

Rock Paper Sissors Server

A very simple server that will accept messages for rock - r, paper - p, and sissors - s and 
let you know the results in a 5 character width string in the format of: Score, P1 Ans, P2 Ans.

## Compilation
* This has compiled on OSX and Red Hat, no garuntees elsewhere.
* Make sure you have a cc command.

```bash
make
```

## Usage

```bash
rpsd
```
